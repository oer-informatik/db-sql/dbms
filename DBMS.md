## Datenbankmanagementsysteme

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/dbms</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/109449766986480012</span>

> **tl/dr;** _(ca. 5 min Lesezeit): Datenbankmanagementsysteme organisieren den Zugriff auf Datenbanken. Welche marktläufigen Systeme gibt es, welche Aufgaben übernehmen sie und mit welcher Software kann ich darauf zugreifen?_


### Begriffsklärung: Datenbank, Datenbanksystem, Datenbankmanagementsystem

Ein Datenbanksystem besteht aus zwei Komponenten: die eigentlichen Daten (Datenbank), und eine Software, die den Zugriff darauf verwaltet (Datenbankmanagementsystem). Eine in Betrieb befindliche "Datenbank" zur Rechnungserstellung ist also ein _Datenbanksystem_ - denn es besteht aus der Datenbank (die eigentlichen gespeicherten Werte zu Rechnungen) und dem Datenbankmanagementsystem (z.B. MySQL), dass den Zugriff verwaltet. Es gibt Datenbankmanagementsysteme für relationale Datenbanken (die auf Tabellen und Fremdschlüsselbeziehungen aufbauen) ebenso wie für andere Datenbanken (Key-Value-Stores wie Browser-Caches, Documentstores, Graph-Datenbanken...). Im folgenden wollen wir uns zunächst auf die relationalen Datenbanken beschränken.

### Aufgaben von realtionalen Datenbankmanagmentsystemen (rDBMS)

Die Aufgaben eines Datenbankmanagementsystems gehen über die reine Verwaltung von Daten hinaus. Der Datenbanktheoretiker Edgar F. Codd hat eine Reihe weiterer Verantwortlichkeiten beschrieben ^[Leider liegt mit keine Primärquelle hierfür vor, daher kann ich diese Liste nicht zitieren]. Heute werden häufig die folgenden Aufgaben Datenbankmanangementsystemen zugewiesen:

- **Datenintegration**: Alle gespeicherten Daten werden einheitlich durch das DBMS verwaltet. der Daten
- **Datenoperation**: Das DBMS bietet Operationen zum erzeugen, suchen und ändern von Daten an (enspricht den CRUD-Operationen: create, read, update, delete)
- **Datenkatalog**: Im Katalog ist die Beschreibung der Datenbankstruktur auslesbar und veränderbar.
- **Benutzersichten**: Das DBMS ermöglicht unterschiedlichen Anwendungen unterschiedliche Darstellungsformen der Daten.
- **Konsistenzüberwachung**: Das DBMS stellt Funktionen bereit, die die Datenintegrität sichern.
- **Zugriffskontrolle**: Es muss möglich sein, unberechtigten Zugriff auf Daten zu unterbinden.
- **Transaktionen**: Zusammenfassung von Änderungen als unteilbare Einheiten, um inkonsistente Zwischenstadien einer DB zu verhindern.
- **Synchronisation**: Konkuriernde Transaktionen ermöglichen, um Inkonsistenzen aus konkurrierenden Lese- und Schreibvorgängen zu verhindern.
- **Datensischerung**: Die Wiederherstellung (versehendlich) gelöschter Daten muss ermöglicht werden.

### Marktläufige realtionale Datenbankmanagmentsysteme

Es gibt eine große Zahl relationaler Datenbankmanagementsysteme. Genaue Zahlen über deren Verbreitung sind schwer zu ermitteln, eine Liste findet sich zum Beispiel unter [db-engines.com](https://db-engines.com/de/ranking).

Beschränkt man die Auswahl auf relationale SQL-DBMS, so gehören zu den wichtigsten derzeit:

- Oracle (Marktführer bei rDBMS)

- MySQL (gehört mittlerweile zu Oracle) - gängig in XAMP-Umgebungen, mittlerweile häufig durch MariaDB ersetzt

- MariaDB (Nachfolgeprojekt des MySQL-Initiators, nach Übernahme von MySQL von Oracle)

- Microsoft SQL Server (Dev und Express-Edition kostenlos)

- PostgreSQL: (OpenSourceDB unter allen gängigen Systemen verbreitet)

- DB2 (IBM)

- MS Access (im Office-Paket enthalten, SQL-fähig, i.d.R. nur SingleUser-fähig, auch als Frontend für andere rDBMS einsetzbar)

- SQLite (wird i.d.R. als in Applikationen eingebettetes RDBMS verwendet - z.B. auch in Java)

### Installation eines DBMS

Datenbankmanangementsysteme können auf verschiedene Art genutzt werden:

- lokal am Client installiert (v.a. zu Testzwecken / zu Entwicklungszwecken).

- in einem Container installiert (z.B. Docker, Podman)

- auf einem Server installiert.

#### Lokale Installation

Zu Testzwecken ist eine lokale Installation bzw. eine Installation in Containern sinnvoll. Ich versuche in den SQL-Artikeln mittelfristig zu den verbreitetsten rDBMS Syntax und Tutorials bereitzustellen. In diesem Zusammenhang bin ich für jeden Tip/Hinweis dankbar!

Downloadlinks und Anleitungen für lokal installierbare DBMS finden sich unter den folgenden Links:

|   | MSSQL |MySQL / MariaDB | postgreSQL  | OracleDB | SQLite|
| -------- | -------- | -------- |-------- |-------- |-------- |
|Downloadlink zur lokalen Installation| [Dev-Edition 550MB](https://www.microsoft.com/de-DE/sql-server/sql-server-downloads)| [MariaDB](https://mariadb.com/downloads/) | [ca. 200 MB](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads)| [OracleDB XE](https://www.oracle.com/database/technologies/appdev/xe.html)|[SQLite (1MB)](https://www.sqlite.org/download.html)

#### Installation in Containern

Ein bisschen mehr Flexibilität zum Preis von höherer Komplexität erreicht man über [die Installation in Containern](https://oer-informatik.de/docker-grundlagen). Ich habe im Folgenden eine Reihe von Tutorials erarbeitet, mit deren Hilfe man relativ schnell lauffähige DB-Container erstellt hat.


|   | MSSQL |MySQL / MariaDB | postgreSQL  | OracleDB | SQLite|
| -------- | -------- | -------- |-------- |-------- |-------- |
|Tutorials zur Erstellung von Containern|[SQL-Server-Container](https://oer-informatik.de/docker-install-mssql)|[MySQL/ MariaDB Container](https://oer-informatik.de/docker-install-mariadb) | [postgres Container](https://oer-informatik.de/docker-install-postgres) | [OracleDB Container](https://oer-informatik.de/docker-install-oracle) (leider nicht mehr aktuell)| [SQLite Container](https://oer-informatik.de/docker-install-sqlite) (nicht sinnvoll)
|Größe der Container| 1,8 GB|MySQL: 724 MB<br/>Maria: 384 MB||||

SQLite ist eine extrem ressourcensparende Datenbank ohne TCP-Client, daher ergibt die Installation in Containern hier am wenigsten Sinn.

Es gibt gleichlautende Tutorials zu [MariaDB/MySQL](https://oer-informatik.de/docker-install-mariadb), [MS-SQL](https://oer-informatik.de/docker-install-mssql), [postgresql](https://oer-informatik.de/docker-install-postgres) sowie (leider nicht mehr aktuell): [Oracle](https://oer-informatik.de/docker-install-oracle) und [SQLite](https://oer-informatik.de/docker-install-sqlite).



### Frontends

Viele DBMS bieten neben einem Kommandozeilen-Client, der häufig bereits Bestandteil der Serverinstallation ist noch Schnittstellen, über die externe Clients mit GUI genutzt werden können. Häufig gibt es ein Standard-Frontend des DBMS-Herstellers. Einige Frontends können mit unterschiedlichen DBMS interagieren, andere sind spezifisch auf ein System zugeschnitten. Anbei eine Übersicht:

|   | MSSQL |MySQL / MariaDB | postgreSQL  | OracleDB | SQLite|
| -------- | -------- | -------- |-------- |-------- |-------- |
|Standard-Frontend des DBMS| [SQL-Server Management Studio (550MB)](https://docs.microsoft.com/de-de/sql/ssms/download-sql-server-management-studio-ssms)| [MySQL Workbench (40MB)](https://dev.mysql.com/downloads/workbench/) |[pgadmin (80MB)](https://www.pgadmin.org/download/) |[SQL Developer (440 MB)](https://www.oracle.com/de/database/sqldeveloper/)|[SQLiteBrowser OpenSource](https://sqlitebrowser.org/dl/)
|weitere gebräuchliche Frontends| | [phpMyAdmin](https://dev.mysql.com/downloads/workbench/) | [Client-Liste](https://wiki.postgresql.org/wiki/PostgreSQL_Clients)|APEX Online|[SQLite Administrator](http://sqliteadmin.orbmu2k.de/) |
[HeidiSQL (Win)](https://www.heidisql.com/download.php)| + | + | + |- |+ |
[SquirrelSQL ](http://www.squirrelsql.org/) nutzt Java/JDBC (Win/Linux/Mac)| + | + | + |+ |+ |
[Valentina Studio](http://valentina-db.com) (Win/Linux/Mac)| + | + | + |- |+ |
[Dbeaver](https://dbeaver.io/download/) (Java, daher Win/Linux/Mac)| + | + | + |+ |+ |



### Links und weitere Informationen

- [Wikibooks-Seite zu Abweichungen der SQL-Dialekte ggü. dem SQL-ANSI-Standard](https://en.wikibooks.org/wiki/SQL_Dialects_Reference/)
- [Übersichtsranking der geläuftigsten Datenbankmanagementsysteme](https://db-engines.com/de/ranking)

