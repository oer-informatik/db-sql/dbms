## Oracle-Testserver mit Docker als Linux-Container installieren


<span class="hidden-text">https://oer-informatik.de/docker-install-oracle</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Oracle ist der Platzhirsch auf dem Markt der relationalen Datenbankmanagement-Systeme (rDBMS). Leider ist es nicht ganz so einfach, einen Oracle-DB-Container ans Laufen zu bekommen - häufig ändern sich die Voraussetzungen und Lizenzbedingungen._

Es gibt gleichlautende Tutorials zu [MariaDB/MySQL](https://oer-informatik.de/docker-install-mariadb), [MS-SQL](https://oer-informatik.de/docker-install-mssql), [postgresql](https://oer-informatik.de/docker-install-postgres), [Oracle](https://oer-informatik.de/docker-install-oracle) und [SQLite](https://oer-informatik.de/docker-install-sqlite).

### Voraussetzungen schaffen

1. Docker muss [installiert und gestartet (link)](https://docs.docker.com/install/) sein.

2. Um Oracle-Datenbank-Container nutzen zu können wird ein [Docker-Account im Docker Hub (link)](https://hub.docker.com/) benötigt.

3. Im Terminal / der PowerShell mit dem Docker-Login anmelden (statt `<name>` den eigenen Benutzernamen eintragen):
```shell
 $ docker login -u <name>
```

### Einen Container aus dem Oracle Image erzeugen und starten

Das Image als Dienst im Hintergrund starten:

```shell
$ docker run --name=meineOracleDB1 -d -p 3310:1521 -p 5531:5500 -e ORACLE_PWD=_hier%1GutesPWnu+2en container-registry.oracle.com/database/free:23.4.0.0-lite
```

Es gibt ein paar Optionen, die angepasst werden sollten:


Option | Bedeutung
--- | :---
`--name=XYZ` | setzt den Containernamen auf _XYZ_ (individualisierbar); sinnvoll ist Zahl am Ende des Containernamens, um mehrere Container des gleichen Images unterschieden zu können
`-d` | detach: wird im Hintergrund weiter ausgeführt
`-p 3310:1521 -p 5531:5500` | Der Port 1152 des Containers wird auf den Port 3310 des hosts gemappt.^[In diesen Beispielen mappe ich die Ports unterschiedlicher DBMS auf die lokalen Ports 3307-3312, da diese durch kein lokales DBMS belegt sind, aber direkt hinter dem MySQL-Standardport 3306 folgen. Jeder andere freie Port ist ebenso möglich, muss nur bei der DB-Verbindung anpasst werden.
`-e ORACLE_PWD=geheim123` | Hier kann ein Passwort für die DB gesetzt werden. Dieses ist natürlich über die Befehls-Historie einsehbar (Sicherheitsrisiko!)
`container-registry.oracle.com/database/free:23.4.0.0-lite` | nutzt das heruntergeladene Image; Der  _tags_ nach dem Doppelpunkt gibt eine bestimmte Version an, z.B. die Version mit mehr Funktionsumfang ohne `-lite` am Ende.

Als Rückgabewert erscheint nach einiger Zeit der Hash-Wert, über den (neben dem selbstgewählten Namen) der Container zukünftig angesprochen werden kann. In meinem Fall:

> 9e268b33a9b7...

Im UML-Sequenzdiagramm sieht der Ablauf etwa wie folgt aus (natürlich weicht der Docker-Befehl in diesem Beispiel ab, da MS SQL-Server im Diagramm geladen wird):

![Imagesuche, Containerbau und -start im UML-Sequenzdiagramm](images/MSSQL-Docker.png)


Es wird eine Containerinstanz erzeugt, die im Hintergrund läuft (über die Option -d wird der Container als _detached_ ausgeführt).

Zunächst startet der Container und die Oracle-Datenbank. Der Startvorgang ist beendet und die Datenbank einsatzbereit, sobald der STATUS "healthy" bei folgendem Befehl erscheint (während des Startvorgangs steht dort `health: starting`):

```shell
$ docker container ls
```

```
PS C:\Users\hanne\Documents\oracledocker> docker container ls
CONTAINER ID   IMAGE                                                       COMMAND                  CREATED          STATUS                            PORTS                                            NAMES
9e268b33a9b7   container-registry.oracle.com/database/free:23.4.0.0-lite   "/bin/bash -c $ORACL…"   13 seconds ago   Up 9 seconds (health: starting)   0.0.0.0:3310->1521/tcp, 0.0.0.0:5531->5500/tcp   meineOracleDB1
```

### Am Container anmelden und die SQL-Konsole starten

Um ein Programm auf dem Container aufzurufen, muss ein Befehl nach dem Muster ` docker exec -it <Containername> /pfad/zum/programm/im/container` abgesetzt werden, wobei die Option `-it` sicherstellt, dass nach Aufruf ein interaktives Terminal Eingaben entgegennimmt. Um eine Konsoleneingabe über die `bash` vornehmen zu können lautet der Befehl:

```shell
$ docker exec -it meineOracleDB1 /bin/bash
```

Ein SQL-Terminal (sqlplus) ist über den Befehl `sqlplus` in der Container-Bash zu erreichen, vom Gastgebersystem aus erreichbar über:

```shell
$ docker exec -it meineOracleDB1 /opt/oracle/product/23ai/dbhomeFree/bin/sqlplus
```
Wichtig ist, mit dem Benutzernamen (Standard: `system`) direkt die Rolle (hier: `sysdba`) mitzuübergeben (also `system as sysdba`):

```
SQL*Plus: Release 23.0.0.0.0 - Production on Sun Jun 30 12:49:23 2024
Version 23.4.0.24.05

Copyright (c) 1982, 2024, Oracle.  All rights reserved.

Enter user-name: system as sysdba
Enter password:
```

Über das Konsolenprogramm und die Eingabe von `<Benutzername>/<Passwort>` (im obigen Beispiel also `system as sysdba` und `_hier%1GutesPWnu+2en`) kann man sich an der Konsole anmelden. Ein schmutziger Weg wäre es, in der Container-Bash direkt `sqlplus system/_hier%1GutesPWnu+2en@FREE as sysdba` einzugeben, dann wäre aber das Passwort in der `history` einsehbar.

```
Connected to:
Oracle Database 23ai Free Release 23.0.0.0.0 - Develop, Learn, and Run for Free
Version 23.4.0.24.05

SQL>
```

Es meldet sich die SQL-Eingabeaufforderung. Mit `exit` kann diese verlassen werden.

### Ein paar Abfragen ausprobieren

Welche Version der Oracle-DB wird genutzt?

```sql
SELECT * FROM v$version;
```

Oracle sollte danach preisgeben, welche Version genutzt wird.

```
Oracle Database 23ai Free Release 23.0.0.0.0 - Develop, Learn, and Run for Free
Version 23.4.0.24.05
```

Das Kommandozeilentool kann als Fall-Back immer genutzt werden - aber wir wollen eigentlich anders arbeiten:

### Zugriff per Frontend (am Beispiel DBeaver)

Komfortabler ist natürlich die Bedienung per Frontend. Die oben gewählten Benutzerdaten und Ports müssen in die Anmeldemaske des DB-Frontends eingetragen werden. Der Ablauf ist immer derselbe und wird hier einmal exemplarisch mit dem Frontend DBeaver umgesetzt.

Das Frontend DBeaver tut sich (unter Windows) manchmal ein bisschen schwer, wenn es darum geht, die korrekte Java-JVM zu finden. Bei mir hat geholfen, in der Datei `dbeaver.ini` direkt im Programmverzeichnis (Windows: `C:\Program Files\DBeaver`) folgende Zeilen einzufügen. Wichtig: `-vm` muss in einer gesonderten Zeile stehen, der Pfad zur aktuellen `jvm.dll` in der darauf folgenden Zeile.

Anpassungen in der `dbeaver.ini

```ìni
-vm
C:\Program Files\Java\jdk-17.0.4\bin\server\jvm.dll
```

Wenn DBeaver startet, muss zunächst eine "Neue Verbindung" zum Container eingerichtet werden:

![Menü Datenbank/Neue Verbindung oder Verbindungssymbol klicken](images/dbeaver/dbeaver_neue_verbindung.png)

Dann muss der richtige Treiber für das jeweilige DBMS ausgewählt werden (ggf. startet dann ein Download):

![Verbindungstyp Oracle wählen](images/dbeaver/dbeaver_verbindungstyp_auswaehlen_oracle.png)

Danach müssen die Benutzerdaten und der Port eingegeben werden (der Port wurde beim Containererstellen oben von 1521 auf 3310 umgeleitet). Der Standarduser ist `system` und das Passwort wurde weiter oben individuell vergeben (und hoffentlich nicht einfach "`_hier%1GutesPWnu+2en`" kopiert). Die Datenbank dieser Version heißt "FREE", auch das muss konfiguriert werden.

![Verbindungsdaten eingeben](images/dbeaver/dbeaver_connection_settings_oracle.png)

Wenn alles geklappt hat, findet sich links in der Spalte die Datenbanknavigation, in der später alle Datenbanken auswählbar sind, im großen Fenster lässt sich über das Menü ein SQL-Script öffnen. 

![SQL-Editor über das Menü öffnen über Rechtsklick auf Verbindungssymbol - SQL-Editor - Neues SQL-Skript](images/dbeaver/dbeaver_erster-sql-befehl1.png)

In das Fenster können dann die eigentlichen SQL-Befehle eingegeben werden (hier als Beispiel: `SELECT * FROM v$version;` in blau). Ausgeführt werden die Befehle dann mit dem "Play"-Symbol (grün), woraufhin in der unteren Hälfte das Ergebnis der Abfrage erscheint.

![Einen SQL-Befehl eingeben und über das Play-Symbol links ausführen](images/dbeaver/dbeaver_erster-sql-befehl2.png)

### Frontend installieren

Das Oracle-Frontend zur Datenbankbearbeitung nennt sich "Oracle SQL-Developer" und kann hier geladen werden:
[Oracle SQL-Developer (erfordert Login, ca. 416MB)](https://www.oracle.com/tools/downloads/sqldev-downloads.html)

Die Verbindung zur Datenbank im Container wird über "Neue Verbindung" erstellt:

![Neue Verbindung im Oracle SQL-Developer erstellen](images/OracleSQLDeveloper-01.png)

In der folgenden Maske müssen die Zugangsdaten eingegeben werden - in unserem obigen Beispiel wären es als SID `FREE` statt `ORCL`:

![Verbindungsdaten eingeben - Achtung: bei "SID" muss in unserem Fall "FREE" stehen, nicht ORCL!!!](images/OracleSQLDeveloper-02.png)

Oracle bietet auch ein zugehöriges Tool zur Datenbank-Modellierung an, das unter folgendem Link geladen werden kann:`

[Oracle SQL Developer Data Modeler (316MB)](https://www.oracle.com/tools/downloads/sql-data-modeler-downloads.html)


### Oracle im Docker-Container automatisch mit Daten füllen

Oft ist es hilfreich, wenn man unmittelbar bei Container-Erstellung ein File mit SQL-Befehlen ausführen kann - beispielsweise um direkt eine Testdatenbank verfügbar zu haben. Im einfachsten Fall kann das erreicht werden, in dem man ein Verzeichnis des Hostsystems innerhalb des Containers als "Volume" mountet (es also im Container unter einem definierten Pfad einbindet). Wenn dieses Volume unter `/docker-entrypoint-initdb.d` eingebunden wird und eine SQL-Datei enthält, so wird diese dann direkt in der DB ausgeführt. Aber Vorsicht: auch alle `*.sh`-Dateien werden direkt ausgeführt.

Die Option, um die unser Befehl oben ergänzt werden muss, lautet:

```shell
-v /lokaler/Pfad/zur/SQLOrdner:/docker-entrypoint-initdb.d
```

Am einfachsten ist es, sich direkt in dem Verzeichnis zu befinden, dass eingefügt werden soll. Dann kann das aktuelle _Workingdirectory_ direkt mit dem `PWD`-Befehl ("Print working directory") eingebunden werden. Die Syntax ist in der PowerShell etwas anders als in der Bash:

Linux oder *nix/Bash

```shell
-v $(pwd):/docker-entrypoint-initdb.d
```

Windows/Powershell:

```powershell
-v ${PWD}:/docker-entrypoint-initdb.d
```

Man also in ein neues Verzeichnis wechselt, in dem eine Datei `setup.sql` liegt (und man mit `docker container stop` sichergestellt hat, dass keine anderen Container diesen Namen oder Port belegen), könnte auf einem Windows-Rechner in der PowerShell der Container gestartet werden mit:

```shell
$ docker run --name=meineOracleDB2 -d -p 3311:1521 -p 5532:5500 -e ORACLE_PWD=_hier%1GutesPWnu+2en -v ${PWD}:/docker-entrypoint-initdb.d container-registry.oracle.com/database/free:23.4.0.0-lite
```

Eine einfache Beispieldatei zum Import findet sich [hier](https://gitlab.com/oer-informatik/db-sql/dbms/-/blob/main/oracle/setup_importtest_oracle.sql)


### weitere Infos...

- [Anleitungen und Infos zu den existierenden Oracle-Containern gibt es hier unter Database](https://container-registry.oracle.com)

- [Anleitung zu Oracle-Docker Containern im Oracle-Github-Repo](https://github.com/oracle/docker-images/blob/main/OracleDatabase/SingleInstance/README.md)

- [Nutzung von Oracle Datenbanken in Docker Containern (Von: Ralf Durben)](https://apex.oracle.com/pls/apex/germancommunities/dbacommunity/tipp/6241/index.html#Fertige_Docker_images)



