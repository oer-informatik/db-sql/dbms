#!/bin/bash
# customized from https://github.com/microsoft/mssql-docker/blob/master/linux/preview/examples/mssql-customize/
# added ability to import multiple sql-files

DBSTATUS=1       # 0 means all DBs are online
ERRCODE=1        # 0 means no errors
LOOPCOUNT=0      # how many 10s sequences are needet to boot DBMS?
LOOPCONDITION=1  # becomes 0 if DBMS is reachable

echo ""
echo ""
echo "[entrypoint SQL] Importscript for SQL-Files"
echo "#=========================================="
echo ""
echo "Waiting 20s for DB to start..."
sleep 20
echo "[entrypoint SQL] Trying to connect to DB prior to start imports"


while [ ${LOOPCONDITION} -eq 1 ]; do
        if [ ${LOOPCOUNT} -lt 20 ]; then      # trying max 20 times (200s)
            LOOPCOUNT=$((LOOPCOUNT+1))       # incrementing no of loops

            # This query checks, whether all db are online, see
            # https://docs.microsoft.com/en-us/sql/relational-databases/system-catalog-views/sys-databases-transact-sql?view=sql-server-2017
            DBSTATUS=$(/opt/mssql-tools/bin/sqlcmd -h -1 -t 1 -U sa -P "$MSSQL_SA_PASSWORD" -Q "SET NOCOUNT ON; Select SUM(state) from sys.databases")
            ERRCODE=$?  # in case of errors
            echo "[entrypoint SQL] Checked DBMS: Loop ${loopcount} Waiting for DBs: ${DBSTATUS} / Error-Code ${ERRCODE} (0 means ok)"
        else
            echo "[entrypoint SQL] Tried 20 times (200 seconds) - Imports aborted"
            exit 1
        fi

        if [ ${DBSTATUS} -eq 0 ] && [ ${ERRCODE} -eq 0 ]; then
            LOOPCONDITION=0
            echo "Everthing seems fine - starting Import"
		else
            echo "[entrypoint SQL] Wait another 10s"
		    sleep 10
        fi
done
echo ""
echo "#==================================================="
echo "Trying to gather all SQL-files in working-directory."
echo ""
for FILENAME in ./*.sql; do  #Regex Filterung nach Dateiendung und Sortierung
   echo ""
   echo "[entrypoint SQL] File ${FILENAME}"
   echo "----------------------------------------"
   echo "Found file ${FILENAME}, sanitizing name (whitespaces)"
   FILENAME=$(echo "${FILENAME}" | sed 's/ /\\ /g')
   echo "Importing file as ${FILENAME}"
   /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P "$MSSQL_SA_PASSWORD" -d master -i "${FILENAME}"
   echo "Finished import of ${FILENAME}"
done

echo ""
echo ""
echo "[entrypoint SQL] All imports finished - container status should change to healthy now"
echo "#==================================================================="