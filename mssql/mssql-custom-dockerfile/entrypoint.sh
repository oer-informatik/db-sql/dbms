#!/bin/bash
# https://github.com/microsoft/mssql-docker/tree/master/linux/preview/examples/mssql-customize

echo "Importiere SQL-Kommandos"
# Start the script to create the DB and user
/usr/config/configure-db.sh &

echo "Starte SQL-Server"
# Start SQL Server
/opt/mssql/bin/sqlservr

