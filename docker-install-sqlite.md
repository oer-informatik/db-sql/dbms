## SQLite-Testserver mit Docker als Linux-Container installieren

<span class="hidden-text">https://oer-informatik.de/docker-install-sqlite</span>

> **tl/dr;** _(ca. 20 min Lesezeit): Von allen DBMS macht es bei SQLite sicher am wenigsten Sinn, es in einem Container ans Laufen zu bringen. Da ich aber die relevantesten DBMS für meine Anleitungen nutze, sollte auch SQLite dabei sein. Also: welche Befehle sind nötig, um es in einem Container zustarten?_

Es gibt gleichlautende Tutorials zu [MariaDB/MySQL](https://oer-informatik.de/docker-install-mariadb), [MS-SQL](https://oer-informatik.de/docker-install-mssql), [postgresql](https://oer-informatik.de/docker-install-postgres) sowie (leider nicht mehr aktuell): [Oracle](https://oer-informatik.de/docker-install-oracle) und [SQLite](https://oer-informatik.de/docker-install-sqlite).


Vorneweg: In den allermeisten Fällen würde man SQLite sicher direkt installieren. In unseren Beispielen wurden für alle anderen DBMS Docker-Container erstellt, daher wird das hier auch einmal für Testzwecke mit SQLite durchgeführt. Docker muss [installiert und gestartet](https://docs.docker.com/install/) sein, damit die folgenden Befehle in der Konsole (Powershell/Bash) eingegeben werden können.

### Ein aktuelles Docker Image für SQLite aus einem Dockerfile generieren

Im Dockerhub findet sich kein offizielles DockerImage für SQLite, daher muss ein solches über ein DOCKERFILE erstellt werden.
Ich beziehe mich v.a. auf die folgende Anleitung [DevOpsHeaven: How to create, backup and restore a SQLite database using Docker](https://devopsheaven.com/sqlite/backup/restore/dump/databases/docker/2017/10/10/sqlite-backup-restore-docker.html)

```Dockerfile
FROM <image, dass dem neu erzeugten zugrunde liegt>
RUN <Befehl, der bei der Containererzeugung ausgeführt werden soll>
COPY <lokaler Pfad> <Pfad im Container>
WORKDIR <Verzeichnis, in dem die folgenden Befehle innerhalb des Containers ausgeführt werden>
ENTRYPOINT ["<Befehl, der auf jeden Fall ausgeführt werden soll>","<unveränderliche Argumente>"]
CMD ["<Default-Argumente des Befehls, die bei docker run überschrieben werden können>"]
```
konkret muss folgendes als eine Textdatei gespeichert werden, die "Dockerfile" heißt:

```Dockerfile
FROM alpine:latest
RUN apk add --update sqlite
RUN mkdir /db
WORKDIR /db

ENTRYPOINT ["sqlite3"]
CMD ["test.db"]
```
Dies ist die "Bauanleitung" für ein Image, aus dem dann später unsere Container erstellt werden können. Basierend auf "Alpine Linux" wird dann im Container `sqlite` gestartet mit der Datenbank `test.db` - und wir sollten direkt in der SQL-Konsole landen.

Das neue Image muss zunächst erzeugt werden. Im Verzeichnis, in dem das `Dockerfile` liegt muss folgendes ausgeführt werden (Punkt am Ende nicht vergessen!):

```shell
docker image build -t mein_sqlite_image:v0.1 .
```
Daraufhin wird einiges geladen und jeder Schritt im Dockerfile ausgeführt. Am Ende wird ein Hashwert (in meinem Beispiel  `ceb7f5592363`) des Images und der übergebene Tag (in meinem Beispiel  `mein_sqlite_image:v0.1`) ausgegeben:

```shell
Sending build context to Docker daemon  2.048kB
Step 1/6 : FROM alpine:latest
latest: Pulling from library/alpine ...
Step 2/6 : RUN apk add --update sqlite ...
Step 3/6 : RUN mkdir /db ...
Step 4/6 : WORKDIR /db ...
Step 5/6 : ENTRYPOINT ["sqlite3"] ...
Step 6/6 : CMD ["test.db"] ...
Successfully built ceb7f5592363
Successfully tagged mein_sqlite_image:v0.1
```
(Die Ausgabe ist stark verkürzt.)

Ob das Image angelegt wurde lässt sich mit `docker image ls` prüfen:
```
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
mein_sqlite_image   v0.1                ceb7f5592363        5 minutes ago       9.33MB
```

Gestartet werden kann der Container mit:

```
docker run --rm -it -v  C:\sqlite\:/db mein_sqlite_image:v0.1 test.db
```

Die Datenbank wird als Datei "test.db" unter dem Pfad im Host-Dateisystem gespeichert, die vor den `:/db` notiert ist - in meinem Fall `C:\sqlite\`. Nach dem Starten des Containers meldet sich die SQL-Konsole:

```
SQLite version 3.32.1 2020-05-25 16:19:56
Enter ".help" for usage hints.
sqlite>
```

Zum herumprobieren nutze ich das Beispiel von DevOpsHeaven:
```
sqlite> create table test_table(id int, description varchar(10));
sqlite> .tables
test_table
sqlite> insert into test_table values(1, 'foo');
sqlite> insert into test_table values(2, 'bar');
sqlite> select * from test_table;
1|foo
2|bar
sqlite> .exit
```

Eine Übersicht über alle Befehle der SQLite-Konsole findet sich hier: https://sqlite.org/cli.html

SQLite bietet keine TCP-Schnittstelle, daher kann die DB nur über die Datenbank-Datei genutzt werden. HeidiSQL bietet beispielsweise die Möglichkeit, SQLite-Dateien zu öffnen.


### Einen Datenbank-Dump erstellen und zurückspielen

Beim Starten des Containers können direkt SQLite-Befehle übergeben werden. Beispielsweise kann der Befehl `.dump` genutzt werden, um die Gesamte Datenbank als SQL-Befehl zu exportieren:

```
docker run --rm -it -v  C:\sqlite\:/db mein_sqlite_image:v0.1 test.db .dump >> dump.sql
```
Die Text-Datei dump.sql mit allen SQL-Befehlen findet sich daraufhin im aktuellen Arbeitsverzeichnis.

Andersherum kann auch ein kompletter SQL-Dump in die bestehende DB zurückgespielt werden:
```
cat dump.sql | docker run --rm -i -v  C:\sqlite\:/db mein_sqlite_image:v0.1 test.db
```


### weitere Infos...
[DevOpsHeaven: How to create, backup and restore a SQLite database using Docker](https://devopsheaven.com/sqlite/backup/restore/dump/databases/docker/2017/10/10/sqlite-backup-restore-docker.html)

