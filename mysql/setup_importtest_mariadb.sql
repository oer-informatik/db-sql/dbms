CREATE DATABASE importtest;

USE importtest;

CREATE TABLE importtest.planeten(
id INT AUTO_INCREMENT PRIMARY KEY,
name CHAR(255),
bahnradius FLOAT,
mondzahl INT,
masse FLOAT,
durchmesser FLOAT
);

INSERT INTO importtest.planeten (name, bahnradius, mondzahl, masse, durchmesser)
VALUES('Merkur', 57909175, 0, 3.3e23, 2439),
('Venus', 108208930, 0, 4.86e24, 6051),
('Erde', 149597890, 1, 5.97e24, 6378),
('Mars', 227936640, 2, 0.641e24, 3397),
('Jupiter', 778412020, 79, 1898e24, 71492),
('Saturn', 1426725400, 82, 568e24, 60267),
('Uranus', 2870972200, 27, 86e24, 25559 ),
('Neptun', 4498252900, 14, 102e24, 24764);

